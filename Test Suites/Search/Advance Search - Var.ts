<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Advance Search - Var</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>5</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8841f0b5-0551-4091-a594-c6d4bc52389f</testSuiteGuid>
   <testCaseLink>
      <guid>592db4ed-37b4-4cb8-9d96-10c9e07d4c2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search Test Case/Advance Search - Price</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>0e6d0327-8c2f-430d-b481-cd7fd61ac198</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Search/Advance Search - Price</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>0e6d0327-8c2f-430d-b481-cd7fd61ac198</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>price</value>
         <variableId>f959c7fb-b41f-422c-b50e-9e0fd4d1eded</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
