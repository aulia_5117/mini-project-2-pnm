<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>err_length</name>
   <tag></tag>
   <elementGuidId>2013d750-44f6-4263-abc8-ba73267323e0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>strong.warning</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>00cbe157-33af-4ae9-9793-5a36f3aac881</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>warning</value>
      <webElementGuid>0988165a-a8a9-4f69-805a-22db534801c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    Search term minimum length is 3 characters
                </value>
      <webElementGuid>7749a82d-4d49-4cf1-b970-fbdc31f2334c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/strong[@class=&quot;warning&quot;]</value>
      <webElementGuid>3f971476-af3a-494e-8495-053a2e794a28</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search In product descriptions'])[1]/following::strong[1]</value>
      <webElementGuid>3d77dbc9-167e-465c-9a5f-0b61b60a2e15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/preceding::strong[1]</value>
      <webElementGuid>a5090bd2-037d-41b3-aad5-f6ba8353afd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::strong[1]</value>
      <webElementGuid>4d2752de-0a9a-487f-95b4-35c6b0ec8756</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Search term minimum length is 3 characters']/parent::*</value>
      <webElementGuid>6c351829-2050-4f2a-9e28-009a649299b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/strong</value>
      <webElementGuid>0efee982-80f9-4eba-925e-c59f6c1fd9cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = '
                    Search term minimum length is 3 characters
                ' or . = '
                    Search term minimum length is 3 characters
                ')]</value>
      <webElementGuid>f06ff91c-4c82-4c61-a8d1-0d5744647dbe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
