import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

def facility = 'Seoul CURA Healthcare Center'

boolean hospital_readmission = true

def healthcare_program = 'Medicaid'

def visit_date = '16/02/2024'

def comment = 'comment, like and subscribe!'

WebUI.callTestCase(findTestCase('reusable-test/block-login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.selectOptionByValue(findTestObject('Object Spy/Appointment/drop_facility'), facility, false)

if (hospital_readmission == true) {
    WebUI.check(findTestObject('Object Spy/Appointment/chk_hospital_readmission'))
}

WebUI.click(findTestObject('Object Spy/Appointment/radio_programs_Medicaid'))

def select_radio = healthcare_program

switch (select_radio) {
    case select_radio = 'Medicare':
        println(select_radio)

        WebUI.click(findTestObject('Object Spy/Appointment/radio_programs_Medicare'))

        break
    case select_radio = 'Medicaid':
        println(select_radio)

        WebUI.click(findTestObject('Object Spy/Appointment/radio_programs_Medicaid'))

        break
    case select_radio = 'None':
        println(select_radio)

        WebUI.click(findTestObject('Object Spy/Appointment/radio_programs_None'))

        break
    default:
        println(select_radio)

        WebUI.click(findTestObject('Object Spy/Appointment/radio_programs_None'))

        break
}

WebUI.setText(findTestObject('Object Spy/Appointment/input_visit_date'), visit_date)

WebUI.acceptAlert()

WebUI.setText(findTestObject('Object Spy/Appointment/input_comment'), comment)

WebUI.click(findTestObject('Object Spy/Appointment/btn_Book Appointment'))

WebUI.waitForElementVisible(findTestObject('Object Spy/AppointmentConfirmation/h2_AppointmentConfirmation'), 0)

WebUI.verifyElementVisible(findTestObject('Object Spy/AppointmentConfirmation/p_subtitleText'))

WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_facility'), facility)

if (hospital_readmission == true) {
    WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_hospital_readmission'), 'Yes')
} else {
    WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_hospital_readmission'), 'No')
}

switch (select_radio) {
    case select_radio = 'Medicare':
        println(select_radio)

        WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_program'), 'Medicare')

        break
    case select_radio = 'Medicaid':
        println(select_radio)

        WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_program'), 'Medicaid')

        break
    case select_radio = 'None':
        println(select_radio)

        WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_program'), 'None')

        break
    default:
        println(select_radio)

        WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_program'), 'None')

        break
}

WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_visit_date'), visit_date)

WebUI.verifyElementText(findTestObject('Object Spy/AppointmentConfirmation/lbl_comment'), comment)

if (true) {
    WebUI.closeBrowser()
}

