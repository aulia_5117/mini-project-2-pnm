<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_category</name>
   <tag></tag>
   <elementGuidId>f744d211-87fd-43bc-abab-21e5f5a690ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#Cid</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@id='Cid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>bb66b08d-05f6-4fc8-94fd-a34119abb4e8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9e2e38dd-cf25-45c5-b866-3c1ec192c73b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-number</name>
      <type>Main</type>
      <value>The field Category must be a number.</value>
      <webElementGuid>5e84fedc-c5cc-425a-b432-9555508d7007</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>6a2f7984-3c4b-42a6-b8c3-584ed800283e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Cid</value>
      <webElementGuid>ec698518-59ac-442b-8e54-41ed072447d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
</value>
      <webElementGuid>7648832e-87b8-402c-b8f4-27ae64e766b6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Cid&quot;)</value>
      <webElementGuid>c7de37fa-5400-4feb-a364-58834a819020</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@id='Cid']</value>
      <webElementGuid>a445f328-7c17-403c-918b-def2ea3ea491</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='advanced-search-block']/div/select</value>
      <webElementGuid>6c3b60bb-8808-491f-ab1f-82cbeb30d448</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category:'])[1]/following::select[1]</value>
      <webElementGuid>aca2ea1f-4d23-4e03-8f27-df35e7534c58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced search'])[1]/following::select[1]</value>
      <webElementGuid>61af88c0-1123-4dfd-b83f-d0a61030d7ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automatically search sub categories'])[1]/preceding::select[1]</value>
      <webElementGuid>aec3de64-4593-49ca-9f5e-6c03ff386415</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manufacturer:'])[1]/preceding::select[1]</value>
      <webElementGuid>700c9f0e-6b0f-46db-be09-8b04e868d974</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//select</value>
      <webElementGuid>3f89709e-7033-406d-ab74-af127a155226</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@id = 'Cid' and @name = 'Cid' and (text() = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
' or . = 'All
Books
Computers
Computers >> Desktops
Computers >> Notebooks
Computers >> Accessories
Electronics
Electronics >> Camera, photo
Electronics >> Cell phones
Apparel &amp; Shoes
Digital downloads
Jewelry
Gift Cards
')]</value>
      <webElementGuid>7a31a0bd-e6cf-434e-9578-4c87b119aeec</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
