<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>class_product-info</name>
   <tag></tag>
   <elementGuidId>3f7bbd25-a32a-4b47-b818-276d5a33c6b4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.item-box</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[contains(@class,&quot;product-item&quot;)]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>e1c71c23-8ead-4bd4-b3be-1b942f78e44a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>item-box</value>
      <webElementGuid>27bce645-7547-4cf9-bb69-3b874b8aa08d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            </value>
      <webElementGuid>6a1f5fbc-84de-48f1-b45b-514841ccafc9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-results&quot;]/div[@class=&quot;product-grid&quot;]/div[@class=&quot;item-box&quot;]</value>
      <webElementGuid>d7ee5809-be4e-437a-9c50-ef5df34df316</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='per page'])[1]/following::div[3]</value>
      <webElementGuid>9ac348cd-6f2a-4e54-a7bb-31473d4e38ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Display'])[1]/following::div[3]</value>
      <webElementGuid>a5b020d2-1ff1-4ef7-97d4-e6ee477cede3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div/div</value>
      <webElementGuid>49d147a4-e9e3-46d7-9a70-c0a523bf6bbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            ' or . = '
                                

    
        
            
        
    
    
        
            Build your own cheap computer
        
            
                
                    
                    
                
            
        
            Build it
        
        
            
                800.00
            
            
                
                    
            
            
        
    


                            ')]</value>
      <webElementGuid>2bcbb262-bc08-41ec-919e-4cbf128c7dd7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
