<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_testingpnmgmail.com</name>
   <tag></tag>
   <elementGuidId>8c6f1e7a-58ac-4b85-9313-0364be7467e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.account</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'testingpnm@gmail.com')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>bc9de028-d34c-48ba-b6e2-c682b6b1916c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/customer/info</value>
      <webElementGuid>ebb98113-d183-4fda-a9a8-81d294ca5dbe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>account</value>
      <webElementGuid>f52f2b8e-b0e3-4845-acb4-2f9936d5598e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>testingpnm@gmail.com</value>
      <webElementGuid>53c94022-3215-4f6d-8829-90d623ac5c3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;header&quot;]/div[@class=&quot;header-links-wrapper&quot;]/div[@class=&quot;header-links&quot;]/ul[1]/li[1]/a[@class=&quot;account&quot;]</value>
      <webElementGuid>8c804551-6136-41eb-9b17-f9ab7e7237d6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'testingpnm@gmail.com')]</value>
      <webElementGuid>563530ed-3053-43d0-9151-ec18cccc11aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Log out'])[1]/preceding::a[1]</value>
      <webElementGuid>77717b0c-9b85-4940-b5a3-231cbd2a8e08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Shopping cart'])[1]/preceding::a[2]</value>
      <webElementGuid>ea7ed551-4af7-4ef3-90b3-f54fadab882b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='testingpnm@gmail.com']/parent::*</value>
      <webElementGuid>4a25bf95-cf18-4673-acfc-1a5e0ced6e14</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/customer/info')]</value>
      <webElementGuid>9eb9c658-8db9-4b57-b5d1-43566ed71c46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li/a</value>
      <webElementGuid>2108e20d-0c70-4a59-b640-93e062fdf5dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/customer/info' and (text() = 'testingpnm@gmail.com' or . = 'testingpnm@gmail.com')]</value>
      <webElementGuid>911e95ce-d409-46e3-8e92-a5f2f1ffd953</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
