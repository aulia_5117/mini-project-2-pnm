<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_firstname</name>
   <tag></tag>
   <elementGuidId>186b07b5-dfa8-4b01-8412-52ab64b9d187</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#FirstName</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='FirstName']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>620c9eb3-09f4-432a-bf00-d46ffef500de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>text-box single-line</value>
      <webElementGuid>05462aae-41e6-4e90-8102-06c3a1359866</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>9947ca42-e73f-474e-b7a3-1a6597fde6c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-val-required</name>
      <type>Main</type>
      <value>First name is required.</value>
      <webElementGuid>2af12da7-d7f6-4fe8-9cd3-55862b45e5e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>3a61eca4-b702-4330-a938-045fdf0c7cc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>FirstName</value>
      <webElementGuid>56756c46-24a9-4963-9c31-4857f010dbc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>0f71adcc-28e8-4a11-9752-a3fc83c09d57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;FirstName&quot;)</value>
      <webElementGuid>5e3fdb80-7fe0-4910-86ff-5d41d1970095</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='FirstName']</value>
      <webElementGuid>47721f57-7a35-4d77-99e3-902f8532994c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/input</value>
      <webElementGuid>312df16a-19a2-451a-b29b-84e9261f6e4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'FirstName' and @name = 'FirstName' and @type = 'text']</value>
      <webElementGuid>e8f232bb-db60-44cf-80b0-96f855be4917</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
