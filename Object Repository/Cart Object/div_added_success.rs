<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_added_success</name>
   <tag></tag>
   <elementGuidId>a447ac89-cde7-48f7-ac44-81ddbbb39748</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bar-notification</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='bar-notification']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>45541a85-f16d-45ae-9840-7341dbb2839b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>bar-notification</value>
      <webElementGuid>25a6823e-869e-402f-95af-3cb96cd8d642</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bar-notification success</value>
      <webElementGuid>1099cb56-939d-40ab-9dda-1103d7070c97</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
     
The product has been added to your shopping cart</value>
      <webElementGuid>c3b237fe-dd8c-4ae2-b65c-43716a0d7138</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bar-notification&quot;)</value>
      <webElementGuid>9efca155-7780-456c-b6fe-e34503b15bdc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='bar-notification']</value>
      <webElementGuid>742a9b4f-19d4-4566-8292-3020d46aca76</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='testingpnm@gmail.com'])[1]/preceding::div[2]</value>
      <webElementGuid>b821197e-caf7-4d29-86bc-e93abd72fe09</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]</value>
      <webElementGuid>0e209d57-f507-4fbd-a6e2-c535654c89f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'bar-notification' and (text() = '
     
The product has been added to your shopping cart' or . = '
     
The product has been added to your shopping cart')]</value>
      <webElementGuid>7d58f1d5-1063-4694-929e-ab6f0a6f43a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
