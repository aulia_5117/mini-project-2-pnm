import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('HomePage/a_login'))

WebUI.setText(findTestObject('Object Repository/Credentials Object/input_email'), email)

def emailVar = email

WebUI.setText(findTestObject('Object Repository/Credentials Object/input_password'), password)

def passwordVar = password

WebUI.click(findTestObject('Object Repository/Credentials Object/btn_login'))

if ((emailVar == '') || (passwordVar == '')) {
    WebUI.verifyElementVisible(findTestObject('Object Repository/Credentials Object/Error/err_login'))
    WebUI.closeBrowser()
    return
}

def loginIncorrect = WebUI.verifyElementVisible(findTestObject('Object Repository/Credentials Object/Error/err_login'), 
    FailureHandling.OPTIONAL)

if (loginIncorrect == true) {
	WebUI.closeBrowser()
	return
}

WebUI.verifyElementVisible(findTestObject('Object Repository/Credentials Object/a_logout'))

WebUI.closeBrowser()

