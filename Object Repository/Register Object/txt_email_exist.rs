<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txt_email_exist</name>
   <tag></tag>
   <elementGuidId>f06d1647-2f46-402e-9760-cdc9cdfbe4d9</elementGuidId>
   <imagePath>Screenshots/Targets/Page_Demo Web Shop. Register/li_The specified email already exists.png</imagePath>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'The specified email already exists' or . = 'The specified email already exists')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > ul > li</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value>Screenshots/Targets/Page_Demo Web Shop. Register/li_The specified email already exists.png</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[(text() = 'The specified email already exists' or . = 'The specified email already exists')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>04f88f68-1454-439d-b78e-8d7c3cc330b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The specified email already exists</value>
      <webElementGuid>eb0109bd-4dc6-4d07-9f98-f74cbfaa160e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/form[1]/div[@class=&quot;page registration-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/ul[1]/li[1]</value>
      <webElementGuid>d9840ebe-7fa0-437c-b16c-1eb74c47e471</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[2]/following::li[1]</value>
      <webElementGuid>bcaf8c94-6e22-4fc2-b42b-bdb4cd47e8fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wait...'])[1]/following::li[1]</value>
      <webElementGuid>31fc7421-cf9b-4f6b-9eb3-fec14630ca5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Personal Details'])[1]/preceding::li[1]</value>
      <webElementGuid>f7960d42-f7cf-475a-82c9-d3203c0d4d4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Gender:'])[1]/preceding::li[1]</value>
      <webElementGuid>20092077-fa9e-4dc8-8ff4-7498756f5a02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The specified email already exists']/parent::*</value>
      <webElementGuid>3119ebe0-494f-413b-9a11-7684194c1fc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/ul/li</value>
      <webElementGuid>304c850f-c949-481d-ba85-9a22640f9c5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = 'The specified email already exists' or . = 'The specified email already exists')]</value>
      <webElementGuid>e1375c81-6234-4ca3-834e-05d6e3cc8651</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
