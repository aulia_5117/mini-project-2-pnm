<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Basic Search</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>5</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e552fd46-5083-4276-8b55-6f9400ad2675</testSuiteGuid>
   <testCaseLink>
      <guid>e7391396-afcd-4c12-9acd-0779081c07ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Search Test Case/Basic Search</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>891b558f-a0d5-4110-a683-72de8985cca2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Search/Basic Search</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>891b558f-a0d5-4110-a683-72de8985cca2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>keyword</value>
         <variableId>eacdaaf1-9ec6-4a26-b103-0c51cbb64529</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
