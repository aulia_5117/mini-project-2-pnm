<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>err_login</name>
   <tag></tag>
   <elementGuidId>ae313e84-7a70-4d5e-9cf7-722ed1b37ff2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.validation-summary-errors > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7459150b-e9d5-4ece-a896-8db82df11061</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Login was unsuccessful. Please correct the errors and try again.</value>
      <webElementGuid>e55aaebf-49c7-4818-8bb3-d7cea542f1a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page login-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;customer-blocks&quot;]/div[@class=&quot;returning-wrapper&quot;]/div[@class=&quot;form-fields&quot;]/form[1]/div[@class=&quot;message-error&quot;]/div[@class=&quot;validation-summary-errors&quot;]/span[1]</value>
      <webElementGuid>0e66d717-5055-4b8b-95b2-0058fe6c1167</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Returning Customer'])[1]/following::span[1]</value>
      <webElementGuid>a3a49c6f-e89f-4627-bae7-24c73c8c4c96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Customer'])[1]/following::span[1]</value>
      <webElementGuid>486eba73-595c-4bb0-b844-d5d10becd21b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No customer account found'])[1]/preceding::span[1]</value>
      <webElementGuid>242863ad-4adb-47be-b662-25873529d0ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email:'])[1]/preceding::span[1]</value>
      <webElementGuid>8228c286-a744-44c5-9dd5-b2ae665f9511</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Login was unsuccessful. Please correct the errors and try again.']/parent::*</value>
      <webElementGuid>443176e6-a72d-402f-b791-db6590bbb879</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/span</value>
      <webElementGuid>7c346d83-ea8b-40f0-86bc-38f1050b538b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Login was unsuccessful. Please correct the errors and try again.' or . = 'Login was unsuccessful. Please correct the errors and try again.')]</value>
      <webElementGuid>7078474a-e7c1-4acd-a398-a746f81bff10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
