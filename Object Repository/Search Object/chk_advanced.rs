<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>chk_advanced</name>
   <tag></tag>
   <elementGuidId>b033ab5c-db9a-43cc-8e9b-ab682756a257</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.inputs.reversed > label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Search keyword:'])[1]/following::label[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>92c68416-8e36-477d-a321-f58d62ce3c64</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>As</value>
      <webElementGuid>29c1ec68-9beb-43bf-817d-9ec0714d0e9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Advanced search</value>
      <webElementGuid>a0587d9e-4ff7-4b50-ac55-38427366db90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/div[@class=&quot;page search-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;search-input&quot;]/form[1]/div[@class=&quot;basic-search&quot;]/div[@class=&quot;inputs reversed&quot;]/label[1]</value>
      <webElementGuid>3e986ac1-9e06-4c16-91b2-b7118ff80d5b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search keyword:'])[1]/following::label[1]</value>
      <webElementGuid>72afec7c-8278-4129-acca-28a181eca98a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Search'])[1]/following::label[2]</value>
      <webElementGuid>6d9f5520-f5af-4402-9daf-6618c32f10a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Category:'])[1]/preceding::label[1]</value>
      <webElementGuid>8a42620f-eca8-43ba-ac58-6ebe2b3ab4e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Automatically search sub categories'])[1]/preceding::label[2]</value>
      <webElementGuid>a5d61387-9c94-48e8-ac6d-01b0d9d41bcf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Advanced search']/parent::*</value>
      <webElementGuid>a1e6aecf-0dfa-46bf-98b4-104a7a239431</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/label</value>
      <webElementGuid>f266f685-4308-4398-9483-e47ce1130b9b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Advanced search' or . = 'Advanced search')]</value>
      <webElementGuid>51a52e45-d345-403e-8491-dc8e0d319148</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
