import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


WebUI.openBrowser('https://demowebshop.tricentis.com/')

WebUI.click(findTestObject('HomePage/a_register'))

WebUI.click(findTestObject('Object Repository/Register Object/input_gender_male'))

WebUI.setText(findTestObject('Object Repository/Register Object/input_firstname'), firstName)
def firstNameVar = firstName

WebUI.setText(findTestObject('Object Repository/Register Object/input_lastname'), lastName)
def lastNameVar = lastName

WebUI.setText(findTestObject('Object Repository/Register Object/input_email'), email)
def emailVar = email

WebUI.setText(findTestObject('Object Repository/Register Object/input_password'), password)
def passwordVar = password

WebUI.setText(findTestObject('Object Repository/Register Object/input_confirm_password'), confirmPassword)
def confirmPasswordVar = confirmPassword

WebUI.click(findTestObject('Object Repository/Register Object/btn_register'))

if (firstNameVar == "" || lastNameVar == "" ||  emailVar == "" || passwordVar == "" || confirmPasswordVar == "") {
	if (firstNameVar == "" ) {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Register Object/Error/err_firstname'))
		WebUI.closeBrowser()
		return	
	} else if (lastNameVar == "") {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Register Object/Error/err_lastname'))
		WebUI.closeBrowser()
		return
	} else if (emailVar == "") {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Register Object/Error/err_email'))
		WebUI.closeBrowser()
		return
	} else if (passwordVar == "" || confirmPasswordVar == "") {
		WebUI.verifyElementVisible(findTestObject('Object Repository/Register Object/Error/err_password'))
		WebUI.closeBrowser()
		return
	}
}

def check = WebUI.verifyElementVisible(findTestObject('Register Object/txt_email_exist'))

if (check == true) {
    WebUI.closeBrowser()
} else {
    WebUI.verifyElementVisible(findTestObject('Object Repository/Register Object/div_reg_complete'))

    WebUI.click(findTestObject('Object Repository/Register Object/btn_reg_continue'))
}

WebUI.closeBrowser()

